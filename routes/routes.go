package routes

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"goforce/api"
)

func BindRoutes(api *api.API, echo *echo.Echo) {
	users := echo.Group("/users")
	users.POST("/signup", api.UserSignup)
	users.POST("/login", api.UserLogin)

	userInfo := users.Group("/info")
	userInfo.Use(middleware.JWT([]byte("knrjkevdckjh")))
	userInfo.GET("/", api.UserInfo)
}
