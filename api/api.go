package api

import (
	"goforce/models"
)

type API struct {
	users *models.UserManager
}

func NewAPI(db *models.DB) *API {
	usermgr, _ := models.NewUserManager(db)

	return &API{
		users: usermgr,
	}
}
