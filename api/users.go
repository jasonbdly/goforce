package api

import (
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

var signingKey = []byte("knrjkevdckjh")

func (api *API) UserSignup(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")

	if username == "" || password == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Username and password cannot be blank.")
	}

	if api.users.HasUser(username) {
		return echo.NewHTTPError(http.StatusBadRequest, "Username already taken.")
	}

	user := api.users.AddUser(username, password)

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = user.Username
	claims["uuid"] = user.UUID
	claims["exp"] = time.Now().Add(time.Minute * 1).Unix()

	t, err := token.SignedString(signingKey)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}

func (api *API) UserLogin(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")

	fmt.Println("Username: ", username)

	user := api.users.FindUser(username)
	if user.Username == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Username not found.")
	}

	if !api.users.CheckPassword(user.Password, password) {
		return echo.NewHTTPError(http.StatusBadRequest, "No match found for given username and password.")
	}

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = user.Username
	claims["uuid"] = user.UUID
	claims["exp"] = time.Now().Add(time.Minute * 1).Unix()

	t, err := token.SignedString(signingKey)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}

func (api *API) UserInfo(c echo.Context) error {
	userToken := c.Get("user").(*jwt.Token)
	claims := userToken.Claims.(jwt.MapClaims)
	uuid := claims["uuid"].(string)
	user := api.users.FindUserByUUID(uuid)
	return c.JSON(http.StatusOK, user)
}

/*func (api *API) GetUserFromContext(req *http.Request) *models.User {
	userclaims := auth.GetUserClaimsFromContext(req)
	user := api.users.FindUserByUUID(userclaims["uuid"].(string))
	return user
}

func (api *API) UserInfo(w http.ResponseWriter, req *http.Request) {
	user := api.GetUserFromContext(req)
	js, _ := json.Marshal(user)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}*/
