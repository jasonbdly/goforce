package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	_ "github.com/mattn/go-sqlite3"

	"goforce/api"
	"goforce/models"
	"goforce/routes"
)

func main() {
	db := models.NewSqliteDB("storage.db")
	api := api.NewAPI(db)

	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Static("/", "goforce-ui/dist")

	routes.BindRoutes(api, e)

	e.Logger.Fatal(e.Start(":9000"))
}
